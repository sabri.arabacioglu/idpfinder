<%--
  Copyright 2010-2017 ForgeRock AS. All Rights Reserved

  Use of this code requires a commercial software license with ForgeRock AS.
  or with one of its affiliates. All use shall be exclusively subject
  to such license between the licensee and ForgeRock AS.
--%>

<%@ page import="com.sun.identity.shared.encode.Base64" %>
<%@ page import="com.sun.identity.saml2.common.SAML2Constants" %>
<%@ page import="com.sun.identity.saml2.common.SAML2Utils" %>
<%@ page import="org.owasp.esapi.ESAPI"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="java.util.List" %>
<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>

<html lang="tr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
  <title>SSO</title>  
  <style>

    article,aside,details,figcaption,figure,footer,header,hgroup,img.koc-logo,menu,nav,section{display:block}a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font:inherit;vertical-align:baseline}body{line-height:1;color:#878787;overflow:hidden}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}html{background-color:#ededed;background-image:url(images/bg.jpg);-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;background-repeat:no-repeat;background-position:center top;background-attachment:fixed}img.koc-logo{max-width:280px;margin:auto}.form-container{max-width:400px;margin:5% auto;padding:10px}.form-container form{margin-top:80px}.form-container .form-item{margin:15px 0;padding:8px 15px;border-radius:12px;font-size:17pt;outline:0;font-family:Catamaran,sans-serif}.form-container .center{text-align:center}form input,form span{display:block;width:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.form-container .btn{background:#ef483e;border:0;color:#f7fffb;cursor:pointer;font-size:17pt}.btn:hover{box-shadow:1px 1px 3px 0 rgba(239,72,62,.7)}.btn:focus{background:#fff;color:#ef483e;box-shadow:inset 0 0 0 1px rgba(239,72,62,.7),1px 1px 3px 0 rgba(239,72,62,.7)}.text-box{border:1px solid #a9a9a9;background-color:rgb(240,240,240,.6);color:rgba(0,0,0,.7);font-weight:200}.text-box:focus{box-shadow:inset 0 0 2px 1px rgba(239,72,62,.3);border-color:rgba(239,72,62,.8)}.form-container .description{width:102%;padding:0;margin:30px -1%;text-shadow:1px 1px 4px #ededed,1px -1px 4px #ededed,-1px 1px 4px #ededed,-1px -1px 4px #ededed;font-size:13.5pt;line-height:16pt}@-ms-viewport{width:device-width}@media all and (max-width:500px){img.koc-logo{max-width:50%}.form-container{margin:15% auto}.form-container form{margin:20% 3% 0}.form-container .form-item{font-size:14pt}.form-container .description{width:108%;margin:30px -4%;font-size:12pt}}@media only screen and (max-height:480px){img.koc-logo{max-width:30%}.form-container{margin:2% auto;max-width:340px}.form-container form{margin:8% 3% 0}.form-container .form-item{font-size:12pt}.form-container .description{width:108%;margin:30px -4%;font-size:12pt}}
  </style>
  <link href="https://fonts.googleapis.com/css?family=Catamaran:200,400" rel="stylesheet">
</head>

<body class="background">
        <%
            List idpList = null;
            String errorURL = "idpfinderError.html";
            String samlIdP = "";
            String relayState = "";
            String idpListSt = "";
            List<String> requestedAuthnContext;

            HttpSession hts = request.getSession();
            if (hts == null) {
        %>
        <jsp:forward page="<%= errorURL %>" />

        <%
            }
            String [] lista = null;
            idpListSt = (String) hts.getAttribute("_IDPLIST_");
            if (idpListSt != null && !idpListSt.isEmpty()) {
               lista =  idpListSt.split(" ");
            } else {
        %>
                <jsp:forward page="<%= errorURL %>" />
        <%
            }

            relayState = (String) hts.getAttribute("_RELAYSTATE_");
            String metaAlias = (String) hts.getAttribute("_IDPMETAALIAS_");
            if (StringUtils.isEmpty(relayState) || StringUtils.isEmpty(metaAlias) ||
                    !SAML2Utils.isRelayStateURLValid(metaAlias, relayState, SAML2Constants.IDP_ROLE)) {
        %>
            <jsp:forward page="<%= errorURL %>" />
        <%
            }

            requestedAuthnContext = (List<String>) hts.getAttribute("_REQAUTHNCONTEXT_");
            if (requestedAuthnContext != null && requestedAuthnContext.isEmpty()) {
        %>
            <jsp:forward page="<%= errorURL %>" />
        <%
            }

            String spRequester = (String) hts.getAttribute("_SPREQUESTER_");
            if (spRequester == null) response.sendRedirect(errorURL);
            if (spRequester.isEmpty()) response.sendRedirect(errorURL);

            samlIdP = request.getParameter("_saml_idp");
            if (!ESAPI.validator().isValidInput("HTTP Parameter Value: " + samlIdP, samlIdP,
                "HTTPParameterValue", 2000, true)){
                samlIdP = null;
            }

            if (samlIdP != null && !samlIdP.isEmpty()) {
                hts.removeAttribute("_IDPLIST_");
                hts.removeAttribute("_RELAYSTATE_");
                hts.removeAttribute("_SPREQUESTER_");
                hts.removeAttribute("_REQAUTHNCONTEXT_");

                if (relayState.indexOf("?") == -1) {
                    relayState += "?";
                } else {
                    relayState += "&";
                }

                 response.sendRedirect(relayState + "_saml_idp=" + samlIdP);
            }

        %>










    <div class="form-container">
    <img src="images/koc-logo.svg" class="koc-logo">
    <form name="myfrm" id="myfrm" action="" method="POST">

          <script language="JavaScript" type="text/javascript">
          $('#myfrm').submit(function()
            {
                $("input[type='submit']", this)
                .val("Please Wait...")
                .attr('disabled', 'disabled');

                return true;
            });

        function accessCookie(cookieName)
        {
          var name = cookieName + "=";
          var allCookieArray = document.cookie.split(';');
          for(var i=0; i<allCookieArray.length; i++)
          {
            var temp = allCookieArray[i].trim();
            if (temp.indexOf(name)==0)
            return temp.substring(name.length,temp.length);
          }
                return "";
        }
          document.getElementById("emailtext").value = accessCookie("forgerockEmailCookie") ;

      </script>

    <input class="text-box form-control form-item" placeholder="E-Posta Adresi" type="text" name="emailtext" id="emailtext">
    <input class="btn form-item" type="submit" value="Giriş">
    <span class="form-item center description">Bu aşamada hesabınızın doğruluğunu test etmek üzere şirketinizin SSO servisine yönlendirileceksiniz.</span>            




    <script language="JavaScript" type="text/javascript">

                function createCookie(cookieName,cookieValue,daysToExpire)
        {
          var date = new Date();
          date.setTime(date.getTime()+(daysToExpire*24*60*60*1000));
          document.cookie = cookieName + "=" + cookieValue + "; expires=" + date.toGMTString();

        }
                function accessCookie(cookieName)
        {
          var name = cookieName + "=";
          var allCookieArray = document.cookie.split(';');
          for(var i=0; i<allCookieArray.length; i++)
          {
            var temp = allCookieArray[i].trim();
            if (temp.indexOf(name)==0)
            return temp.substring(name.length,temp.length);
          }
                return "";
        }
                function checkCookie()
        {
          var cookieEmail = accessCookie("forgerockEmailCookie");
          if (cookieEmail!="" && cookieEmail == "<%= request.getParameter("emailtext") %>"){
                          document.getElementById("emailtext").value = cookieEmail;
          }
                  else
          {
                        cookieEmail="<%= request.getParameter("emailtext") %>";
            if (cookieEmail!="" && cookieEmail!=null)
            {
            createCookie("forgerockEmailCookie", cookieEmail, 30);
            }
          }
        }

</script>
                                <%String email = request.getParameter("emailtext"); %>
                   <%
                                   if (email != null && !email.equals("") && email.contains("@")) {
                        for(String  preferredIDP : lista) {
                     if (lista != null && lista.length > 0) {
                          String preferredIDPB64 = Base64.encode(preferredIDP.getBytes());
                                                  String customerName = "http://" + email.split("@")[1] + "/sso";
if ("http://kocsistem.com.tr/sso".equalsIgnoreCase(customerName)) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;"  value="<%= Base64.encode("http://sts.kocsistem.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

else if (("http://arcelik.com/sso".equalsIgnoreCase(customerName)) || ("http://arcelik.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://beko.com/sso".equalsIgnoreCase(customerName))  || ("http://beko-deutschland.de/sso".equalsIgnoreCase(customerName)) || ("http://beko.fr/sso".equalsIgnoreCase(customerName))|| ("http://beko.co.uk/sso".equalsIgnoreCase(customerName)) || ("http://beko.com.pl/sso".equalsIgnoreCase(customerName))|| ("http://beko.com.tr/sso".equalsIgnoreCase(customerName))|| ("http://beko.ru/sso".equalsIgnoreCase(customerName))|| ("http://beko.ua/sso".equalsIgnoreCase(customerName)) || ("http://arcelik-lg.com/sso".equalsIgnoreCase(customerName)) || ("http://arctic.ro/sso".equalsIgnoreCase(customerName))) { %>


<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://arcelik.okta.com/kochub".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if (("http://otokoc.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://budget.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://birmot.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://otokocotomotiv.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://avis.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://zipcar.com.tr/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://stscrm.otokoc.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}
if ("http://erceaydin.com/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://sts.windows.net/bb0794ea-9a1b-4fd3-baeb-21e26875769a/".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if (("http://akpakoc.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://aygaz.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://aygazdg.com.tr/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.aygaz.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}
if ("http://tupras.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.tupras.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://otokar.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.otokar.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://entekelektrik.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.entekelektrik.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

else if (("http://divan.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://divan.com/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://sso.divan.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://demirexport.com/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.demirexport.com/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("https://t.otokar.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp"  style="display:none;" value="<%= Base64.encode("https://adfs.t.otokar.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://koctas.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp"  style="display:none;" value="<%= Base64.encode("https://adfs.koctas.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}


if (("http://ford.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://fo.ford.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://ford.com/sso".equalsIgnoreCase(customerName)))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp"  style="display:none;" value="<%= Base64.encode("https://login.ford.com.tr".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://kocfinans.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.kocfinans.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://kuh.ku.edu.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.kuh.ku.edu.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://turktraktor.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://sts.turktraktor.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://opet.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.opet.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if (("http://tat.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://duzey.com.tr/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.tat.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if (("http://amerikanhastanesi.org/sso".equalsIgnoreCase(customerName)) || ("http://medamerikan.com/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.amerikanhastanesi.org/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://rmkmarine.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.rmkmarine.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://tofas.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://sts.tofas.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://koc.k12.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://adfs.koc.k12.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://seturmarinas.com/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.seturmarinas.com/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://kocgal.local/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://sts.maximus.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}


if ("http://koczer.com/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://adfs.koczer.com/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://bilkom.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("http://adfs.bilkom.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://setur.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.setur.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://opetfuchs.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.opetfuchs.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://ditasdeniz.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.ditasdeniz.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if (("http://tani.com.tr/sso".equalsIgnoreCase(customerName)) ||("http://paro.com.tr/sso".equalsIgnoreCase(customerName) ))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.tani.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}


if ("http://ku.edu.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.ku.edu.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}


if ("http://koc.com.tr/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.koc.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

if ("http://thyopet.com/sso".equalsIgnoreCase(customerName))  { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.thyopet.com/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}

else if (("http://arter.org.tr/sso".equalsIgnoreCase(customerName)) || ("http://ingage.media/sso".equalsIgnoreCase(customerName)) || ("http://rmk-museum.org.tr/sso".equalsIgnoreCase(customerName))  || ("http://turmepa.org.tr/sso".equalsIgnoreCase(customerName)) || ("http://inventram.com/sso".equalsIgnoreCase(customerName))|| ("http://khev.org.tr/sso".equalsIgnoreCase(customerName)) || ("http://ktsk.com.tr/sso".equalsIgnoreCase(customerName))|| ("http://ramsigorta.com.tr/sso".equalsIgnoreCase(customerName))|| ("http://ram.com.tr/sso".equalsIgnoreCase(customerName))|| ("http://haremlique.com/sso".equalsIgnoreCase(customerName)) || ("http://dp.local/sso".equalsIgnoreCase(customerName)) || ("http://arkinsaat.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://ark.info.tr/sso".equalsIgnoreCase(customerName)) || ("http://digitalpanorama.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://rmkclassic.com/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://central.maximus.com.tr/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}


else if (("http://yapikredi.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://yapikredi.com.az/sso".equalsIgnoreCase(customerName)) || ("http://yapikredi.nl/sso".equalsIgnoreCase(customerName))  || ("http://ykfaktoring.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://ykleasing.com.tr/sso".equalsIgnoreCase(customerName))|| ("http://ykportfoy.com.tr/sso".equalsIgnoreCase(customerName)) || ("http://ykyatirim.com.tr/sso".equalsIgnoreCase(customerName))|| ("http://ykykultur.com.tr/sso".equalsIgnoreCase(customerName))) { %>

<input type="radio" name="_saml_idp" checked id="_saml_idp" style="display:none;" value="<%= Base64.encode("https://adfs.yapikredi.com.tr/adfs/ls/idpinitiatedsignon.htm/adfs/services/trust".getBytes()) %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
<%break;}



if (preferredIDP.equalsIgnoreCase(customerName)) {  %>

<input type="radio" name="_saml_idp" checked id="_saml_idp"  style="display:none;" style="display:none;" value="<%= preferredIDPB64 %>">
<script language="JavaScript" type="text/javascript">
checkCookie()
document.forms["myfrm"].submit();
</script>
 <%break;}}}}%>
                        </form>
                </div>
    </body>
</html>
